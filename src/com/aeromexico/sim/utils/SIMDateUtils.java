package com.aeromexico.sim.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * @author Miguel Mendoza
 *
 */
public class SIMDateUtils {

	private static final int DAYS_TO_ADD = 331;

	public static String getDateProcess(String dateToAdd) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMMyy", Locale.ENGLISH);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateFormat.parse(dateToAdd));
		calendar.add(Calendar.DATE, DAYS_TO_ADD);		
//		calendar.add(Calendar.DATE, (int) (DAYS_TO_ADD - getDaysDifference(dateToAdd, dateFormat)));		
		dateFormat.setTimeZone(calendar.getTimeZone());
		return dateFormat.format(calendar.getTime());
	}

	@SuppressWarnings("unused")
	private static long getDaysDifference(String dateFrom, SimpleDateFormat myFormat) {

		long diff = 0;
		Calendar calendar = Calendar.getInstance();

		try {
			Date date1 = myFormat.parse(dateFrom);
			Date date2 = myFormat.parse(myFormat.format(calendar.getTime()));
			diff = TimeUnit.DAYS.convert(date2.getTime() - date1.getTime(), TimeUnit.MILLISECONDS);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return diff;

	}
}
