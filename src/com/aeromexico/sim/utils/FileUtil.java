package com.aeromexico.sim.utils;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import com.aeromexico.sim.model.RawSIM;

/**
 * @author Miguel Mendoza
 *
 */
public class FileUtil {
	public static List<RawSIM> readSSIMFile(String filePath) throws IOException {
		RandomAccessFile f = new RandomAccessFile(filePath, "r");

		List<RawSIM> rawSSIMList = new ArrayList<RawSIM>();
		RawSIM rawSSIM = null;
		String actualLine = null;

		while ((actualLine = f.readLine()) != null) {
			if (actualLine.startsWith("3")) {
				if (rawSSIM != null) {
					rawSSIMList.add(rawSSIM);
				}
				rawSSIM = new RawSIM();
				rawSSIM.setRecordType3(actualLine);
			} else if (rawSSIM != null && actualLine.startsWith("4")) {
				rawSSIM.getRecordType4().add(actualLine);
			}
		} // End while

		// We add the last record
		if (rawSSIM != null) {
			rawSSIMList.add(rawSSIM);
		}
		f.close();
		return rawSSIMList;
	}
}
