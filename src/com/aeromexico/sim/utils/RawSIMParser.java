package com.aeromexico.sim.utils;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.aeromexico.sim.model.RawSIM;
import com.aeromexico.sim.model.SIMCatalogItem;

/**
 * @author Miguel Mendoza
 *
 */
public class RawSIMParser {

	public static List<SIMCatalogItem> parseRawSSIMList(List<RawSIM> rawSSIMList) throws ParseException {

		Pattern pattern = Pattern.compile("AM[ ][0-9]{1,4}");
		Matcher matcher;

		List<String> type3Strings = new ArrayList<>();
		List<String> type4Strings = new ArrayList<>();
		List<SIMCatalogItem> catalogItems = new ArrayList<>();

		String rowKey3 = "";
		String rowKey4 = "";

		for (RawSIM rawSIM : rawSSIMList) {
			type3Strings.add(rawSIM.getRecordType3());

			for (String type4String : rawSIM.getRecordType4())
				type4Strings.add(type4String);

		}

		for (String type4 : type4Strings) {
			String data = type4.substring(39, 194);
			matcher = pattern.matcher(data);

			if (matcher.find()) {
				String flightAM = matcher.group();
				flightAM = flightAM.replace("AM", "").trim();
				rowKey4 = type4.substring(5, 13);

				for (String type3 : type3Strings) {
					rowKey3 = type3.substring(5, 13);

					if (rowKey4.equals(rowKey3)) {

						String flightDL = type3.substring(5, 9);
						flightDL = flightDL.replace(' ', '0');

						String periodOperationFrom = type3.substring(14, 21);
						String periodOperationTo = type3.substring(21, 28);

						if (periodOperationTo.equals("00XXX00")) {
							periodOperationTo = SIMDateUtils.getDateProcess(periodOperationFrom);
						}

						String frequency = type3.substring(28, 35);
						String departureStation = type3.substring(36, 39);
						String arrivalStation = type3.substring(54, 57);

						createSIMCatalogItems(periodOperationFrom, periodOperationTo, frequency, flightDL, flightAM,
								departureStation, arrivalStation, catalogItems);
					}
				}
			}

		}

		System.out.println("ITEMS: " + catalogItems.size());
		return catalogItems;

	}

	private static void createSIMCatalogItems(String periodOperationFrom, String periodOperationTo, String frequency,
			String flightDL, String flightAM, String departureStation, String arrivalStation,
			List<SIMCatalogItem> catalogItems) throws ParseException {

		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMMyy", Locale.ENGLISH);
		SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyyMMdd");
		DateFormatSymbols dfs = new DateFormatSymbols(Locale.ENGLISH);
		List<String> daysToRetrieve = new ArrayList<>();

		Calendar start = Calendar.getInstance();
		start.setTime(dateFormat.parse(periodOperationFrom));

		Calendar end = Calendar.getInstance();
		end.setTime(dateFormat.parse(periodOperationTo));

		for (char c : frequency.toCharArray()) {
			daysToRetrieve.add(getDayBasedFrecuency(c));
		}

		while (start.before(end) || start.equals(end)) {
			if (daysToRetrieve.contains(dfs.getWeekdays()[start.get(Calendar.DAY_OF_WEEK)])) {
				SIMCatalogItem catalogItem = new SIMCatalogItem();
				catalogItem.setFlightDate(dateFormat2.format(start.getTime()));
				catalogItem.setDeltaFlightNumber(flightDL.trim());
				catalogItem.setAmFlightNumber(flightAM.trim());
				catalogItem.setOrigin(departureStation);
				catalogItem.setDestination(arrivalStation);

				SIMCatalogItem catalogItemAdded = catalogItems.stream()
						.filter(cat -> (catalogItem.getFlightDate().equals(cat.getFlightDate())
								&& catalogItem.getDeltaFlightNumber().equals(cat.getDeltaFlightNumber())
								&& catalogItem.getAmFlightNumber().equals(cat.getAmFlightNumber())
								&& catalogItem.getOrigin().equals(cat.getOrigin())
								&& catalogItem.getDestination().equals(cat.getDestination())))
						.findAny().orElse(null);

				if (catalogItemAdded == null) {
					catalogItems.add(catalogItem);
				}
			}
			start.add(Calendar.DAY_OF_YEAR, 1);
		}
	}

	private static String getDayBasedFrecuency(char c) {
		String day = "";

		switch (c) {
		case '1':
			day = "Monday";
			break;
		case '2':
			day = "Tuesday";
			break;
		case '3':
			day = "Wednesday";
			break;
		case '4':
			day = "Thursday";
			break;
		case '5':
			day = "Friday";
			break;
		case '6':
			day = "Saturday";
			break;
		case '7':
			day = "Sunday";
			break;
		default:
			day = "";
		}

		return day;
	}

}
