package com.aeromexico.sim.model;

/**
 * @author Miguel Mendoza
 *
 */
public enum Days {
	SUNDAY(1, "Sunday"), MONDAY(2, "Monday"), TUESDAY(3, "Tuesday"), WEDNESDAY(4, "Wednesday"), THURSDAY(5,
			"Thursday"), FRIDAY(6, "Friday"), SATURDAY(7, "Saturday");

	private final int value;
	private final String name;

	Days(int value, String name) {
		this.value = value;
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

}
