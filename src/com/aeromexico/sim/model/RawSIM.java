package com.aeromexico.sim.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Miguel Mendoza
 *
 */
public class RawSIM {
	private String recordType3;
	private List<String> recordType4;

	public RawSIM() {
		recordType4 = new ArrayList<String>();
	}

	public String getRecordType3() {
		return recordType3;
	}

	public void setRecordType3(String recordType3) {
		this.recordType3 = recordType3;
	}

	public List<String> getRecordType4() {
		return recordType4;
	}

	public void setRecordType4(List<String> recordType4) {
		this.recordType4 = recordType4;
	}

}
