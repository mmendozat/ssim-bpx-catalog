package com.aeromexico.sim.model;

import java.io.Serializable;

/**
 * @author Miguel Mendoza
 *
 */
public class SIMCatalogItem implements Serializable {

	private static final long serialVersionUID = 214809439124324474L;

	String flightDate;
	String deltaFlightNumber;
	String amFlightNumber;
	String origin;
	String destination;

	public String getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	public String getDeltaFlightNumber() {
		return deltaFlightNumber;
	}

	public void setDeltaFlightNumber(String deltaFlightNumber) {
		this.deltaFlightNumber = deltaFlightNumber;
	}

	public String getAmFlightNumber() {
		return amFlightNumber;
	}

	public void setAmFlightNumber(String amFlightNumber) {
		this.amFlightNumber = amFlightNumber;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destin) {
		this.destination = destin;
	}

	@Override
	public String toString() {
		return flightDate + "|" + deltaFlightNumber + "|" + origin + "|" + destination + "|" + amFlightNumber + "\n";
	}

}
