package com.aeromexico.sim;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.aeromexico.sim.model.RawSIM;
import com.aeromexico.sim.model.SIMCatalogItem;
import com.aeromexico.sim.utils.FileUtil;
import com.aeromexico.sim.utils.RawSIMParser;

/**
 * @author Miguel Mendoza
 *
 */
public class SIMMain {

	/**
	 * @param args
	 * @throws IOException
	 * @throws ParseException
	 */
	public static void main(String[] args) throws IOException, ParseException {

		System.out.println("Comienza a las " + new Date().toString());

		File file = new File("D:\\Users\\Miguel Mendoza\\Downloads\\SIM pruebas\\sims\\ssimdl.110618");

		List<RawSIM> fileSIMItems = FileUtil.readSSIMFile(file.getAbsolutePath());

		List<SIMCatalogItem> items = RawSIMParser.parseRawSSIMList(fileSIMItems);

		File salida = new File("D:\\Users\\Miguel Mendoza\\Downloads\\SIM pruebas\\CAT_DELTA_110618.csv");

		salida.createNewFile();

		FileWriter fileWriter = new FileWriter(salida);

		for (SIMCatalogItem simCatalogItem : items) {
			fileWriter.write(simCatalogItem.toString());
		}

		fileWriter.flush();
		fileWriter.close();

		System.out.println("Termina a las " + new Date().toString());

	}

}
